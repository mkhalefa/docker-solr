#!/bin/bash
mkdir /vifi/docker-solr1/data
sudo chown 8983:8983 /vifi/docker-solr1/data
docker run -it --rm -v /vifi/docker-solr1/data:/target solr1 cp -r server/solr /target/
